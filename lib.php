<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Callbacks used by Moodle API.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Adds a Random activity instance.
 * @param stdClass $instance New activity data from initial mod form.
 * @return int New database entry id.
 */
function randomactivity_add_instance($instance) {
    global $DB, $CFG;
    $instance->seed = round(microtime(true) * 1000); // Automatically seed to timestamp on creation.
    $instance->timemodified = time();
    $newid = $DB->insert_record( 'randomactivity', $instance );

    $instance->id = $newid;

    if ($instance->duedate) {
        // A due date is set, add an event to the calendar.
        require_once(__DIR__ . '/locallib.php');
        require_once($CFG->dirroot . '/calendar/lib.php');
        calendar_event::create( randomactivity_get_calendar_event( $instance ), false );
    }

    randomactivity_grade_item_update($instance);

    return $newid;
}

/**
 * Updates a Random activity instance.
 * @param stdClass $instance Activity data to update from mod form.
 * @return boolean true
 */
function randomactivity_update_instance($instance) {
    global $DB, $CFG;
    if (isset($instance->instance)) {
        $instance->id = $instance->instance;
    }
    $instance->timemodified = time();
    $DB->update_record( 'randomactivity', $instance );

    $cm = get_coursemodule_from_instance( 'randomactivity', $instance->id, $instance->course );
    $instance->cmidnumber = $cm->id;

    if (isset($instance->grade)) {
        // Grade may have changed, update grade item.
        randomactivity_grade_item_update( $instance );
    }

    require_once(__DIR__ . '/locallib.php');
    require_once($CFG->dirroot . '/calendar/lib.php');
    $eventid = $DB->get_field( 'event', 'id', [ 'modulename' => 'randomactivity', 'instance' => $instance->id ] );
    if ($eventid) {
        // There already exists a calendar event for this instance.
        $event = calendar_event::load( $eventid );
        if ($instance->duedate) {
            // A due date is set, update calendar event.
            $event->update( randomactivity_get_calendar_event( $instance ), false );
        } else {
            // No due date is set, delete calendar event.
            $event->delete();
        }
    } else if ($instance->duedate) {
        // A due date is set but no calendar event exists yet, create one.
        calendar_event::create( randomactivity_get_calendar_event( $instance ), false );
    }

    // Update grades.
    randomactivity_update_grades( $instance );
    return true;
}

/**
 * Deletes a Random activity instance.
 * @param int $id Database entry id.
 * @return boolean true
 */
function randomactivity_delete_instance($id) {
    global $DB, $CFG;
    $instance = $DB->get_record( 'randomactivity', [ 'id' => $id ] );
    randomactivity_delete_grade_item( $instance );

    // Delete calendar event if there is one.
    $eventid = $DB->get_field( 'event', 'id', [ 'modulename' => 'randomactivity', 'instance' => $instance->id ] );
    if ($eventid) {
        require_once($CFG->dirroot . '/calendar/lib.php');
        $event = calendar_event::load( $eventid );
        $event->delete();
    }

    $DB->delete_records( 'randomactivity', [ 'id' => $id ] );

    return true;
}

/**
 * Returns the list if Moodle features this module supports.
 * @param string $feature FEATURE_xx constant.
 * @return boolean|null Whether this module supports feature, null if unpsecified.
 */
function randomactivity_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO :
            return false;
        case FEATURE_GROUPS :
            return true;
        case FEATURE_GROUPINGS :
            return true;
        case FEATURE_GRADE_HAS_GRADE :
            return true;
        case FEATURE_GRADE_OUTCOMES :
            return true;
        case FEATURE_BACKUP_MOODLE2 :
            return true;
        case FEATURE_COMPLETION_HAS_RULES:
            return true;
        default :
            if (defined('FEATURE_MOD_PURPOSE') && $feature == FEATURE_MOD_PURPOSE) {
                return MOD_PURPOSE_OTHER;
            }
            return null;
    }
}

/**
 * Creates or updates grade item for a Random activity.
 * @param stdClass $modinstance Database record of Random activity, with extra cmidnumber field.
 * @param array|string $grades (optional) Grades to update. 'reset' means reset grades in gradebook.
 * @return int GRADE_UPDATE_xx constant.
 */
function randomactivity_grade_item_update($modinstance, $grades = null) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    $params = [ 'itemname' => $modinstance->name, 'idnumber' => $modinstance->cmidnumber ];

    if ($modinstance->grade == 0) {
        $params['gradetype'] = GRADE_TYPE_NONE;
    } else if ($modinstance->grade > 0) {
        $params['gradetype'] = GRADE_TYPE_VALUE;
        $params['grademax'] = $modinstance->grade;
        $params['grademin'] = 0;
    } else if ($modinstance->grade < 0) {
        $params['gradetype'] = GRADE_TYPE_SCALE;
        $params['scaleid'] = -$modinstance->grade;
    }

    if ($grades === 'reset') {
        $params['reset'] = true;
        $grades = null;
    }

    return grade_update('mod/randomactivity', $modinstance->course, 'mod', 'randomactivity', $modinstance->id, 0, $grades, $params);
}

/**
 * Updates grades for a Random activity.
 * @param stdClass $modinstance Database record of Random activity, with extra cmidnumber field.
 * @param int $userid (optional) User id, or 0 for all users.
 * @param boolean $nullifnone (optional) If true and no grades are set for given user, set grade to null.
 */
function randomactivity_update_grades($modinstance, $userid = 0, $nullifnone = true) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    if ($modinstance->grade == 0) {
        randomactivity_grade_item_update($modinstance);
    } else if ($grades = randomactivity_get_user_grades($modinstance->id, $userid)) {
        randomactivity_grade_item_update($modinstance, $grades);
    } else if ($userid && $nullifnone) {
        $grade = new stdClass();
        $grade->userid = $userid;
        $grade->rawgrade = null;
        randomactivity_grade_item_update($modinstance, $grade);
    } else {
        randomactivity_grade_item_update($modinstance);
    }
}

/**
 * Deletes grade item for a Random activity.
 * @param stdClass $modinstance Database record of Random activity.
 */
function randomactivity_delete_grade_item($modinstance) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    $params = [ 'deleted' => 1 ];
    grade_update('mod/randomactivity', $modinstance->course, 'mod', 'randomactivity', $modinstance->id, 0, null, $params);
}

/**
 * TODO
 * @param stdClass $data
 */
function randomactivity_reset_userdata($data) {
}

/**
 * Fetchs and returns current grade for given user or all users.
 * @param stdClass $modinstanceid Database entry id.
 * @param int $userid (optional) User id, or 0 for all users.
 * @return array Array of grades, indexed by user id.
 */
function randomactivity_get_user_grades($modinstanceid, $userid = 0) {
    require_once(__DIR__ . '/locallib.php');
    global $DB;
    $modinstance = $DB->get_record( 'randomactivity', [ 'id' => $modinstanceid ] );
    $course = $modinstance->course;
    $cminfo = get_fast_modinfo($course, -1)->get_instances_of('randomactivity')[$modinstance->id];
    if ($userid != 0) {
        $user = new stdClass();
        $user->id = $userid;
        $users = [ $user ];
    } else {
        $context = context_module::instance( $cminfo->id );
        $users = get_enrolled_users($context);
    }
    $grades = [];
    foreach ($users as $user) {
        $gradedetails = [ 'userid' => $user->id, 'rawgrade' => null, 'finalgrade' => null ];
        if ($modinstance->activities != '') {
            $assignedcmid = randomactivity_get_assigned_activity($cminfo, $modinstance->activities, $user->id, $modinstance->seed);
            try {
                $assignedcm = get_fast_modinfo($course, -1)->get_cm($assignedcmid);
            } catch (moodle_exception $e) {
                // Some assigned modules might be corrupted, it should be fixed from activities form - do nothing about it here.
                continue;
            }
            $gradeitems = grade_get_grades($course, 'mod', $assignedcm->modname, $assignedcm->instance, $user->id)->items;
            if (count( $gradeitems ) > 0) {
                $grade = $gradeitems[0];
                $gradevalue = $grade->grades[$user->id];
                if ($gradevalue->grade !== null) {
                    $adjustedgrade = ($gradevalue->grade - $grade->grademin) / ($grade->grademax - $grade->grademin);
                    $gradedetails = (array) $gradevalue;
                    $gradedetails['userid'] = $user->id;
                    $gradedetails['rawgrade'] = randomactivity_compute_grade_from($modinstance, $adjustedgrade);
                }
            }
        }
        $grades[$user->id] = $gradedetails;
    }
    return $grades;
}

/**
 * Returns whether a scale is used by a Random activity.
 * @param int $modinstanceid Database entry id.
 * @param int $scaleid Scale id.
 * @return boolean true if specified Random activity uses specified scale.
 */
function randomactivity_scale_used($modinstanceid, $scaleid) {
    global $DB;
    return $scaleid && $DB->record_exists( 'randomactivity', [ 'id' => $modinstanceid, 'grade' => -$scaleid ] );
}

/**
 * Returns whether a scale is used by any Random activity.
 * @param int $scaleid Scale id.
 * @return boolean true if any Random activity uses specified scale.
 */
function randomactivity_scale_used_anywhere($scaleid) {
    global $DB;
    return $scaleid && $DB->record_exists( 'randomactivity', [ 'grade' => -$scaleid ] );
}

/**
 * Returns details about Random activities for a user within a course.
 * This only contains which activities they are assigned to.
 * @param stdClass $course Course object.
 * @param stdClass $user User object.
 * @param stdClass $cm Course module instance.
 * @param stdClass $modinstance Random activity instance record.
 * @return stdClass Object with info field.
 */
function randomactivity_user_outline($course, $user, $cm, $modinstance) {
    require_once(dirname(__FILE__) . '/locallib.php');
    $outline = new stdClass();
    if ($modinstance->activities == '') {
        $outline->info = '';
    } else {
        $cminfo = get_fast_modinfo($course, -1)->get_cm($cm->id);
        $assignedcmid = randomactivity_get_assigned_activity($cminfo, $modinstance->activities, $user->id, $modinstance->seed);
        $outline->info = '-> ' . randomactivity_activity_icon_and_name(get_fast_modinfo($course, -1)->get_cm($assignedcmid));
    }
    return $outline;
}

/**
 * Prints details about Random activities for a user within a course.
 * This only prints which activities they are assigned to.
 * @param stdClass $course Course object.
 * @param stdClass $user User object.
 * @param stdClass $cm Course module instance.
 * @param stdClass $modinstance Random activity instance record.
 */
function randomactivity_user_complete($course, $user, $cm, $modinstance) {
    echo randomactivity_user_outline($course, $user, $cm, $modinstance)->info;
}

/**
 * Changes Random activity display depending on context.
 * This changes the appearance of the activity on course page for students if corresponding option is set.
 * @param cm_info $cm Course module info of a Random activity.
 */
function randomactivity_cm_info_dynamic(cm_info $cm) {
    global $DB, $USER;
    require_once(dirname(__FILE__) . '/locallib.php');
    try {
        $context = context_module::instance( $cm->id );
        $module = $DB->get_record( 'randomactivity', [ 'id' => $cm->instance ] );
        if ( !has_capability('mod/randomactivity:manage', $context)
                && !has_capability('mod/randomactivity:viewactivities', $context)
                && !has_capability('moodle/course:manageactivities', context_course::instance( $cm->course ))
                && $module->dynamicdisplay) {
            // Change activity appearance to the one it will redirect to.
            $redirectid = randomactivity_get_assigned_activity($cm, $module->activities, $USER->id, $module->seed);
            $redirectcm = get_fast_modinfo($cm->course, -1)->get_cm($redirectid);
            $cm->set_name($redirectcm->get_formatted_name());
            $cm->set_icon_url($redirectcm->get_icon_url());
        }
    } catch (moodle_exception $e) {
        // We may be in a context where $cm can't be initialized, just skip.
        return;
    }
}

/**
 * Definition of Fontawesome icons mapping.
 * @return string[] Fontawesome icons mapping.
 */
function randomactivity_get_fontawesome_icon_map() {
    return [
            'mod_randomactivity:settings' => 'fa-cog',
            'mod_randomactivity:move' => 'fa-arrows-v',
            'mod_randomactivity:remove' => 'fa-remove',
            'mod_randomactivity:warning' => 'fa-warning',
            'mod_randomactivity:add' => 'fa-plus',
            'mod_randomactivity:refresh' => 'fa-refresh',
            'mod_randomactivity:loading' => 'fa-spinner fa-pulse',
            'mod_randomactivity:reassign' => 'fa-random',
            'mod_randomactivity:edit' => 'fa-edit',
            'mod_randomactivity:dropdown' => 'fa-caret-down',
            'mod_randomactivity:dropdowncollapsed' => 'fa-caret-right',
            'mod_randomactivity:search' => 'fa-search',
            'mod_randomactivity:list' => 'fa-list-ul',
            'mod_randomactivity:grades' => 'fa-check-square'
    ];
}

/**
 * Extends settings menu navigation by adding some elements.
 * @param settings_navigation $settings
 * @param navigation_node $node Node to which add elements.
 */
function randomactivity_extend_settings_navigation(settings_navigation $settings, navigation_node $node) {
    global $PAGE;
    if (! isset( $PAGE->cm->id )) {
        return;
    }
    $cmid = $PAGE->cm->id;
    $context = context_module::instance( $cmid );

    // Insert just after "Edit settings" node.
    $keys = $node->get_children_key_list();
    if (count( $keys ) > 1) {
        $insertkey = $keys[1];
    } else {
        $insertkey = null;
    }

    if ( has_capability( 'mod/randomactivity:viewgrades', $context ) ) {
        $gradesurl = new moodle_url('/mod/randomactivity/gradeslist.php', [ 'id' => $cmid ]);
        $gradesnode = $node->create( get_string( 'grades' ), $gradesurl,
                navigation_node::TYPE_ACTIVITY, null, null,
                new pix_icon( 'grades', '', 'mod_randomactivity') );
        $node->add_node($gradesnode, $insertkey);
    }
    if ( has_capability( 'mod/randomactivity:viewactivities', $context ) ) {
        $modsurl = new moodle_url('/mod/randomactivity/index.php', [ 'id' => $PAGE->cm->course ]);
        $modsnode = $node->create( get_string( 'modulenameplural', 'mod_randomactivity' ), $modsurl,
                navigation_node::TYPE_ACTIVITY, null, null,
                new pix_icon( 'list', '', 'mod_randomactivity') );
        $node->add_node($modsnode, $insertkey);
    }
}

/**
 * Provides the action associated with the given calendar event (in our case, go to activity).
 * This method is called for the block myoverview (timeline on dashboard).
 * @param calendar_event $event The calendar event.
 * @param \core_calendar\action_factory $factory
 * @return \core_calendar\local\event\value_objects\action The action associated wiht the given calendar event.
 */
function mod_randomactivity_core_calendar_provide_event_action(calendar_event $event, \core_calendar\action_factory $factory) {
    $cm = get_fast_modinfo($event->courseid, -1)->instances['randomactivity'][$event->instance];
    return $factory->create_instance(
            get_string('gotoactivity', 'core_calendar'),
            new moodle_url('/mod/randomactivity/view.php', [ 'id' => $cm->id ]),
            1,
            $event->timestart >= time()
    );
}

/**
 * Obtains the automatic completion state for this random activity.
 *
 * @param object $course Course
 * @param object $module Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function randomactivity_get_completion_state($course, $module, $userid, $type) {
    global $DB;

    $courseinfo = get_fast_modinfo($course, $userid);
    $cm = $courseinfo->get_cm($module->id);
    $modinstance = $DB->get_record('randomactivity', [ 'id' => $cm->instance ], '*', MUST_EXIST);
    $completion = new completion_info($course);

    if (!$completion->is_enabled() || $cm->completion != COMPLETION_TRACKING_AUTOMATIC || !$modinstance->completiontrackactivity) {
        return $type;
    }

    $relatedcmid = randomactivity_get_assigned_activity($cm, $modinstance->activities, $userid, $modinstance->seed);
    $relatedcm = $courseinfo->get_cm($relatedcmid);
    switch ($completion->get_data($relatedcm, false, $userid)->completionstate) {
        case COMPLETION_COMPLETE:
        case COMPLETION_COMPLETE_PASS:
            return true;
        case COMPLETION_INCOMPLETE:
        case COMPLETION_COMPLETE_FAIL:
            return false;
    }
    return $type;
}
