<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Ajax script to compute seed to balance the number of users between activities.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define( 'AJAX_SCRIPT', true );

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../locallib.php');

require_login();

$outcome = new stdClass();
$outcome->success = true;
$outcome->response = new stdClass();
$outcome->error = '';
try {
    $id = required_param( 'id', PARAM_INT );
    $nactivities = required_param( 'nactivities', PARAM_INT );
    $group = optional_param( 'group', 0, PARAM_INT );

    if ($nactivities == 0) {
        throw new Exception();
    }

    $activities = [];
    for ($i = 0; $i < $nactivities; $i++) {
        $activities[] = $i;
    }
    $context = context_module::instance($id);

    $bestseed = 0;
    $bestdist = PHP_INT_MAX;

    for ($seed = 0; $seed <= 250; $seed++) {
        $assignees = randomactivity_get_assignees($activities, $seed, $context, $group);
        foreach ($assignees as &$cmassignees) {
            $cmassignees = count($cmassignees);
        }
        $mean = array_sum($assignees) / ((float) $nactivities);
        $dist = 0;
        foreach ($assignees as $ncmassignees) {
            $dist += ($ncmassignees - $mean) ** 2;
        }
        if ($dist < $bestdist) {
            $bestdist = $dist;
            $bestseed = $seed;
        }
        if ($bestdist < 1) {
            break;
        }
    }

    $outcome->response->seed = $bestseed;
} catch ( Exception $e ) {
    $outcome->success = false;
    $outcome->error = $e->getMessage();
}
echo json_encode( $outcome );
die();
