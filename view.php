<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main view page. It redirects students to their assigned activity. It allows teachers to edit the activity pool.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/locallib.php');
require_once(dirname(__FILE__) . '/lib.php');

require_login();

global $PAGE, $OUTPUT, $DB, $USER;

$id = required_param('id', PARAM_INT);
$edit = optional_param('edit', 0, PARAM_INT);
$group = optional_param('group', 0, PARAM_INT);

$cm = get_coursemodule_from_id( 'randomactivity', $id );
$course = $DB->get_record( 'course', [ 'id' => $cm->course ] );
$context = context_module::instance( $cm->id );
$module = $DB->get_record( 'randomactivity', [ 'id' => $cm->instance ] );
$cminfo = get_fast_modinfo($course, -1)->get_cm($id);

// Page setup.
$PAGE->set_cm( $cm, $course, $module );
$PAGE->set_context( $context );
$PAGE->set_title( $module->name );
$PAGE->set_pagelayout( 'incourse' );
$PAGE->set_heading( $course->fullname );
$PAGE->set_url( '/mod/randomactivity/view.php', [ 'id' => $id, 'edit' => $edit ] );

if ( !has_capability('mod/randomactivity:manage', $context)
        && !has_capability('mod/randomactivity:viewactivities', $context) ) {
    // Current user has no permission to view nor manage activities (typically a student), redirect to random activity.
    $courseurl = new moodle_url('/course/view.php', [ 'id' => $course->id ]);
    if ($module->activities == '') {
        echo $OUTPUT->header();
        echo $OUTPUT->notification(get_string('noactivityerror', RANDOMACTIVITY), \core\output\notification::NOTIFY_ERROR);
        echo $OUTPUT->continue_button($courseurl);
        echo $OUTPUT->footer();
        die();
    }
    $redirectid = randomactivity_get_assigned_activity($cminfo, $module->activities, $USER->id, $module->seed);
    $redirectcm = get_fast_modinfo($course, -1)->get_cm($redirectid);
    if ($module->duedate > 0 && $module->duedate < time()) {
        echo $OUTPUT->header();
        echo $OUTPUT->notification(get_string('duedateover', RANDOMACTIVITY), 'info');
        echo $OUTPUT->single_button($courseurl, get_string('returntocourse', RANDOMACTIVITY), 'get', [ 'primary' => true ]);
        echo $OUTPUT->single_button($redirectcm->url, get_string('continuetoactivity', RANDOMACTIVITY), 'get');
        echo $OUTPUT->footer();
        die();
    }
    redirect($redirectcm->url);
    die();
}

if (data_submitted()) {
    // Form has been submitted.
    if (confirm_sesskey() && has_capability('mod/randomactivity:manage', $context)) {
        $module->seed = preg_replace('/[^0-9]/', '', required_param('seed', PARAM_RAW));
        $module->activities = required_param('activities', PARAM_RAW_TRIMMED);
        try {
            randomactivity_update_instance($module);
            $message = get_string('changesapplied', RANDOMACTIVITY);
            $messagetype = \core\output\notification::NOTIFY_INFO;
        } catch (dml_exception $e) {
            $message = get_string('dbupdatefailed', 'error');
            $messagetype = \core\output\notification::NOTIFY_ERROR;
        }
    } else {
        $message = get_string('invalidsesskeyorpermission', RANDOMACTIVITY);
        $messagetype = \core\output\notification::NOTIFY_ERROR;
    }
    redirect(new moodle_url('/mod/randomactivity/view.php', [ 'id' => $id ]), $message, null, $messagetype);
    die();
}

$editmode = $edit && has_capability('mod/randomactivity:manage', $context);

if ($editmode) {
    // Javascript modules are only required for edition.
    $PAGE->requires->js_call_amd('mod_randomactivity/activitiesform', 'setup');
    $PAGE->requires->strings_for_js([ 'addactivities', 'addselectedactivities' ], RANDOMACTIVITY);
    $PAGE->requires->strings_for_js([ 'confirm', 'cancel' ], 'moodle');
}

echo $OUTPUT->header();
echo $OUTPUT->heading_with_help(format_string($module->name), 'pluginname', 'randomactivity');
echo $OUTPUT->box_start();

// Group mode info / group selection.
$grouppanel = '';
if ($cminfo->effectivegroupmode != NOGROUPS) {
    if ($cminfo->groupingid > 0) {
        $groupingname = groups_get_all_groupings($course->id)[$cminfo->groupingid]->name;
        $details = get_string('grouping', RANDOMACTIVITY, $groupingname);
    } else {
        $details = get_string('allgroups');
    }
    $grouppanel .= get_string('groupmodedetails', RANDOMACTIVITY, $details);
    $grouppanel .= $OUTPUT->help_icon('groupmode', RANDOMACTIVITY);
} else {
    if (count(groups_get_all_groups($course->id)) > 0) {
        $grouppanel .= get_string('showonlymembersof', RANDOMACTIVITY);
        $grouppanel .= groups_allgroups_course_menu($course, $PAGE->url, true, $group);
    }
}

echo html_writer::nonempty_tag('div', $grouppanel, [ 'class' => 'group-panel mb-3' ]);

// Main activity pool table.
$table = new html_table();
$table->id = 'randomactivity-activities-table';
$table->attributes['class'] = 'generaltable activitytable';

$titleassignees = get_string('assignees', RANDOMACTIVITY);
if ($editmode) {
    $titleassignees .= randomactivity_print_icon( 'refresh', get_string('refresh_help', RANDOMACTIVITY),
                            [ 'class' => 'refresh-assignees clickable ml-1' ] );
}

$table->head = [
        '#',
        get_string('activity'),
        $titleassignees,
        get_string('gradetype', 'core_grades'),
        get_string('status'),
        get_string('actions'),
];
$table->size = [
        '2em',
        null,
        null,
        null,
        '10%',
        '10%',
];

$table->data = [];
if ($module->activities == '') {
    // No activity has been added yet: add a dummy empty row.
    $cell = new html_table_cell();
    $cell->colspan = count($table->head);
    $row = new html_table_row([ $cell ]);
    $row->attributes['class'] = 'empty-row';
    $table->data[] = $row;
} else {
    // Populate table with current activities.
    $i = 0;
    $assignees = randomactivity_get_assignees_formatted($module->activities, $module->seed, $context, $group);
    foreach (explode(' ', $module->activities) as $cmid) {
        $cmassignees = $assignees[$i++];
        try {
            $cminfo = get_fast_modinfo($course, -1)->get_cm($cmid);
        } catch (moodle_exception $e) {
            $cminfo = null;
        }
        $table->data[] = randomactivity_create_activity_table_row($i, $cminfo, $cmassignees, $editmode, $cmid);
    }
}

echo html_writer::table( $table );

// Seed field and global hidden form definition.
$templatedata = new stdClass();
$templatedata->id = $id;
$templatedata->sesskey = sesskey();
$templatedata->group = $group;
$templatedata->activities = $module->activities;
$templatedata->seed = $module->seed;
$templatedata->editmode = $editmode;
$templatedata->seedhelpbutton = $OUTPUT->help_icon('seed', RANDOMACTIVITY);
echo $OUTPUT->render_from_template('mod_randomactivity/activitiesform', $templatedata);

// Activities addition dialog.
if ($editmode) {

    $modtypes = []; // List of module types that can be added.
    $allsections = []; // List of all activities that can be added (and that are added, those will be hidden).

    // Loop over all activities of the course.
    $section = new stdClass();
    $section->num = -1;
    foreach (get_fast_modinfo($course, -1)->get_cms() as $cminfo) {
        $fullname = (string)$cminfo->modfullname;
        // Only the activities that can be added, ie. no randomactivity nor activities with no view page.
        if ($cminfo->modname != 'randomactivity' && $cminfo->url !== null) {
            // Module type.
            if (!isset($modtypes[$fullname])) {
                $modtypes[$fullname] = new stdClass();
                $modtypes[$fullname]->icon = $cminfo->get_icon_url()->out();
                $modtypes[$fullname]->fullname = $fullname;
                $modtypes[$fullname]->name = $cminfo->modname;
            }
            // Section.
            if ($cminfo->sectionnum > $section->num) {
                if ($section->num >= 0 && count($section->activities) > 0) {
                    $allsections[] = $section;
                }
                $section = new stdClass();
                $section->num = $cminfo->sectionnum;
                $section->activities = [];
                $section->name = get_section_name($course->id, $cminfo->sectionnum);
            }
            // Activity.
            $activity = new stdClass();
            $activity->cmid = $cminfo->id;
            $activity->modname = $cminfo->modname;
            $activity->activity = randomactivity_activity_icon_and_name($cminfo, false);
            // Add a hidden table containing only one row. This row will be copied to the main table if the activity is added.
            $hiddentable = new html_table();
            $hiddentable->attributes['class'] = 'activitytable d-none';
            $hiddentable->data = [ randomactivity_create_activity_table_row(0, $cminfo, get_string('none'), $editmode) ];
            $activity->hiddentable = html_writer::table( $hiddentable );
            $section->activities[] = $activity;
        }
    }
    if ($section->num >= 0 && count($section->activities) > 0) {
        $allsections[] = $section;
    }
    ksort($modtypes);

    // Activity search and filters.
    $templatedata = new stdClass();
    $templatedata->moduletypes = array_values($modtypes);
    $templatedata->allsections = $allsections;
    echo $OUTPUT->render_from_template('mod_randomactivity/addactivities', $templatedata);
}

// Main buttons.
$templatedata = new stdClass();
$templatedata->id = $id;
$templatedata->group = $group;
$templatedata->editmode = $editmode;
$templatedata->allowedit = !$editmode && has_capability('mod/randomactivity:manage', $context);
echo $OUTPUT->render_from_template('mod_randomactivity/navigationbuttons', $templatedata);

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
