// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the behavior of the view page for a randomactivity.
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery', 'jqueryui', 'core/url', 'mod_randomactivity/orderabletable'], function($, jqui, url, OrderableTable) {

    /**
     * Reads the table and updates the hidden form field accordingly.
     * Also updates the table attributes and numbering if necessary.
     * @param {jQuery} $table The table to read and update.
     * @param {jQuery} $activities Hidden form field containing activities ids.
     */
    function updateActivities($table, $activities) {
        // In case the table is emptied, create a dummy empty row (and remove it if the table is filled).
        var $tablerows = $table.find('tbody tr');
        if ($tablerows.length > 1) {
            $table.find('.empty-row').remove();
        } else if ($tablerows.length == 0) {
            var ncols = $table.find('thead th').length;
            $table.find('tbody').append('<tr class="empty-row lastrow"><td colspan="' + ncols + '"></td></tr>');
        }

        // Read table content and update the table.
        var newactivities = '';
        $table.find('tbody tr:not(.empty-row)').each(function(i) {
            $(this).find('td:first-child').html(i + 1);
            newactivities += $(this).data('cmid') + ' ';
        })
        .removeClass('lastrow')
        .last().addClass('lastrow');

        // Update hidden form field.
        $activities.val(newactivities.trim());
    }

    /**
     * Sets up the main Random activity view form when in edit mode.
     * It allows the user to add activities to the pool, reorder them and provides seed generation facilities.
     */
    function setup() {
        var $table = $('#randomactivity-activities-table');
        var $form = $('#form-randomactivity-activities');
        var $activities = $form.find('[name=activities]');
        var $seed = $form.find('[name=seed]');
        updateActivities($table, $activities); // Update in case of refresh (modifications should be lost).

        // Setup the drad-and-drop ordering of the table.
        // Also setup for the hidden table so added rows will be ready too.
        OrderableTable.makeOrderable($('.activitytable'), function() {
            updateActivities($table, $activities);
            $form.change();
        });

        // Buttons "Remove" for each row.
        $('.activitytable .removebutton').click(function() {
            $(this).closest('tr').remove();
            updateActivities($table, $activities);
            $form.change();
        });

        // On form change, prompt the user before leaving the page.
        var preventUnsavedChangesUnload = function(event) {
            event.preventDefault();
            event.returnValue = '';
        };
        var changed = false;
        $form.change(function() {
            $('#apply-assign-button').removeAttr('disabled');
            if (!changed) {
                changed = true;
                window.addEventListener('beforeunload', preventUnsavedChangesUnload);
            }
        });

        // Button "Cancel".
        $('#cancel-button').click(function() {
            // The cancel has been explicitely requested, don't prompt the user anymore.
            window.removeEventListener('beforeunload', preventUnsavedChangesUnload);
        });

        // Confirmation dialog upon submit and acitvity re-assigning.
        var $confirmReassignDialog = $('#confirm-reassign').dialog({
            autoOpen: false,
            dialogClass: 'popup-dialog bg-white p-3',
            title: M.util.get_string('confirm', 'moodle'),
            modal: true,
            buttons:
            [{
                text: M.util.get_string('confirm', 'moodle'),
                'class': 'btn btn-primary mx-1',
                click: function() {
                    // The unloading of the page will not lose modifications, don't prompt the user anymore.
                    window.removeEventListener('beforeunload', preventUnsavedChangesUnload);
                    $form.submit();
                }
            },
            {
                text: M.util.get_string('cancel', 'moodle'),
                'class': 'btn btn-secondary mx-1',
                click: function() {
                    $(this).dialog('close');
                }
            }],
            open: function() {
                $(':focus').blur();
            }
        });

        // Button "Apply changes and re-assign".
        $('#apply-assign-button')
        .attr('disabled', 'disabled')
        .click(function() {
            $confirmReassignDialog.dialog('open');
        });

        // Button "Seed to timestamp".
        $('#seed-timestamp').click(function(event) {
            event.preventDefault();
            $seed.val((new Date()).getTime());
            $form.change();
        });

        var baseurl = url.relativeUrl("/mod/randomactivity/ajax");

        // Button "Seed to balance users".
        $('#seed-balance').click(function(event) {
            event.preventDefault();

            var $load = $('.load-icon');
            $load.show();
            $seed.attr('disabled', 'disabled');

            var id = $form.find('[name=id]').val();
            var group = $form.find('[name=group]').val();
            var activities = $activities.val();
            var nactivities = 0;
            if (activities != '') {
                nactivities = activities.split(' ').length;
            }
            $.ajax(baseurl + '/balanceseed.json.php?id=' + id + '&nactivities=' + nactivities + '&group=' + group)
            .done(function(outcome) {
                if (outcome.success) {
                    $seed.val(outcome.response.seed);
                    $form.change();
                }
            })
            .always(function() {
                $seed.removeAttr('disabled');
                $load.hide();
            });
        });

        // Button "Refresh assignees preview".
        $('.refresh-assignees').click(function() {
            var id = $form.find('[name=id]').val();
            var group = $form.find('[name=group]').val();
            var activities = $activities.val();
            var seed = $seed.val();
            $.ajax(baseurl + '/refresh.json.php?id=' + id + '&activities=' + activities + '&seed=' + seed + '&group=' + group)
            .done(function(outcome) {
                if (outcome.success) {
                    $table.find('tbody tr td:nth-child(3)').each(function(i) {
                        $(this).html(outcome.response.assignees[i]);
                    });
                }
            });
        });

        // Activities addition choice filters.
        var filters = {
            $selector: $('.activity-type-selector'),
            $dropdown: $('.activity-type-dropdown'),
            $selectmenu: $('.activity-type-selector, .activity-type-dropdown'),
            $selectedvalue: $('.activity-type-selected span'),
            $search: $('#activity-search')
        };

        // Apply filters and search field to activities choice.
        var applyActivitiesChoiceFilters = function() {
            var type = filters.$selectedvalue.data('cmtype');
            var name = filters.$search.val();
            var currentActivities = $activities.val().split(' ');

            var noSectionToShow = true;
            $('.activities-to-add-list .section').each(function() {
                var noActivityToAddInSection = true;
                var isExpanded = $(this).is('.expanded');
                $(this).find('.activity-to-add').each(function() {
                    // Activity metadata.
                    var cmid = $(this).data('cmid');
                    var cmtype = $(this).data('cmtype');
                    var cmname = $(this).find('label').text();

                    // Apply filters logic.
                    var isFilteredIn = (!type || cmtype == type)
                        && !currentActivities.includes(cmid + "")
                        && (name === '' || cmname.toLowerCase().includes(name.toLowerCase()));

                    // Show or hide activity according to filters.
                    $(this).toggle(isFilteredIn && isExpanded);

                    if (isFilteredIn) {
                        noActivityToAddInSection = false;
                    }
                });

                // Hide section if it is empty with current filters.
                if (noActivityToAddInSection) {
                    $(this).hide();
                } else {
                    noSectionToShow = false;
                    $(this).show();
                }
            });

            // If all sections are hidden, show a message.
            $('#no-activity-to-add-message').toggle(noSectionToShow);
        };

        // Activities addition dialog.
        var $addActivitiesDialog = $('#activities-choice').dialog({
            autoOpen: false,
            dialogClass: 'popup-dialog bg-white p-3',
            title: M.util.get_string('addactivities', 'mod_randomactivity'),
            modal: true,
            buttons:
            [{
                text: M.util.get_string('addselectedactivities', 'mod_randomactivity'),
                'class': 'btn btn-primary mx-1',
                click: function() {
                    $('.activity-to-add').each(function() {
                        if ($(this).find('input[type="checkbox"]').prop('checked')) {
                            $(this).find('tr').clone(true, true).appendTo($table).css('display', '');
                        }
                    });
                    $(this).dialog('close');
                    updateActivities($table, $activities);
                    $form.change();
                }
            },
            {
                text: M.util.get_string('cancel', 'moodle'),
                'class': 'btn btn-secondary mx-1',
                click: function() {
                    $(this).dialog('close');
                }
            }],
            open: function() {
                $(':focus').blur();

                // Uncheck all checkboxes.
                $('.activity-to-add input[type="checkbox"]').prop('checked', false);

                // Expand all sections.
                $('.activities-to-add-list .section').addClass('expanded');
                $('.activities-to-add-list').scrollTop(0);

                // Reset filters.
                filters.$selectedvalue.html($('.activity-type').first().html()).data('cmtype', '');
                filters.$selector.removeClass('expanded').addClass('collapsed');
                filters.$selectmenu.width('');
                filters.$search.val('').keyup();

                // Reset activities choice.
                applyActivitiesChoiceFilters();
            }
        });

        // Button "Add activities".
        $('#add-activities').click(function() {
            $addActivitiesDialog.dialog('open');
        });

        // Activity type filter dropdown menu collapse/expand.
        filters.$selectmenu.click(function() {
            filters.$selector.toggleClass('collapsed expanded');
            if (filters.$selector.is('.expanded')) {
                // Compute width including scrollbar.
                var dropdownwidth = filters.$dropdown.width() * 2 - filters.$dropdown.prop('clientWidth');
                filters.$selectmenu.width(Math.max(dropdownwidth, filters.$selector.width()));
            } else {
                filters.$selectmenu.width('');
            }
        });

        // Activity type filter dropdown menu choice.
        $('.activity-type').click(function() {
            filters.$selectedvalue.html($(this).html()).data('cmtype', $(this).data('cmtype'));
            applyActivitiesChoiceFilters();
        });

        // Activity search field.
        filters.$search.keyup(function() {
            $(this).toggleClass('empty', $(this).val() === '');
            applyActivitiesChoiceFilters();
        });

        $('.search-remove').click(function() {
            filters.$search.val('').keyup();
        });

        $('.search-icon').click(function() {
            filters.$search.focus();
        });

        // Select / Deselect all visible activities of a section.
        $('.activities-to-add-list .section .toggle-all').click(function(e) {
            e.stopImmediatePropagation();
            $(this).parents('.section').find('.activity-to-add:visible input[type="checkbox"]')
            .prop('checked', ($(this).data('action') == 'select'));
        });

        // Expand / Collapse a section.
        $('.activities-to-add-list .section-header').click(function() {
            $(this).parents('.section').toggleClass('expanded');
            applyActivitiesChoiceFilters();
        });
    }

    return {
        setup: setup
    };
});