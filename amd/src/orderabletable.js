// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Provides utility methods to make a html table manually orderable.
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery'], function($) {

    /**
     * Make the given tables orderable, by allowing the user to drag-and-drop rows.
     * Rows need to have at least one element with class 'draghandle' to be used as a handle.
     * The table in which the drag-and-drop occurs is computed dynamically, so cloning rows from a table
     * into another one will make them orderable in the latter.
     * @param {jQuery} $tables Set of tables to make orderable.
     * @param {Function} onChange A function to call every time row order is modified.
     */
    function makeOrderable($tables, onChange) {
        var initialY;
        var prevCss;
        var initialIndex;
        var currentIndex;
        var rowsHeightOffset;
        var $table;
        var $rowBeingDragged = null;

        $tables.find('.draghandle').mousedown(function(event) {
            event.preventDefault();
            initialY = event.pageY;
            $table = $(this).closest('table');
            $rowBeingDragged = $(this).closest('tr');
            $rowBeingDragged.addClass('dragged');
            prevCss = $rowBeingDragged.attr('style') || '';
            initialIndex = $rowBeingDragged.index();
            currentIndex = initialIndex;
            rowsHeightOffset = 0;
        });

        $(window).mousemove(function(event) {
            if ($rowBeingDragged !== null) {
                event.preventDefault();

                var toplimit, bottomlimit;
                var justmoved = false;
                if (currentIndex > 0) {
                    var $toprow = $table.find('tbody tr:nth-child(' + currentIndex + ')');
                    toplimit = ($toprow.offset().top * 2 + $toprow.height() + $rowBeingDragged.height()) / 2;
                    if (event.pageY < toplimit) {
                        $rowBeingDragged.insertBefore($toprow);
                        rowsHeightOffset += $toprow.height();
                        currentIndex--;
                        justmoved = true;
                    }
                }
                if (currentIndex < $table.find('tbody tr').length - 1 && !justmoved) {
                    var $bottomrow = $table.find('tbody tr:nth-child(' + (currentIndex + 2) + ')');
                    bottomlimit = ($bottomrow.offset().top * 2 + $bottomrow.height() - $rowBeingDragged.height()) / 2;
                    if (event.pageY > bottomlimit) {
                        $rowBeingDragged.insertAfter($bottomrow);
                        rowsHeightOffset -= $bottomrow.height();
                        currentIndex++;
                    }
                }

                var cssOffset = event.pageY - initialY + rowsHeightOffset;
                $rowBeingDragged.css('position', 'relative');
                $rowBeingDragged.css('top', cssOffset);
            }
        }).mouseup(function() {
            if ($rowBeingDragged !== null) {
                $rowBeingDragged.attr('style', prevCss);
                $rowBeingDragged.removeClass('dragged');
                $rowBeingDragged = null;
                if (currentIndex != initialIndex) {
                    onChange();
                }
            }
        });
    }

    return {
        makeOrderable: makeOrderable
    };
});