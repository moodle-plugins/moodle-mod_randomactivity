<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Observer for grade, grade items and completion changes in other activities.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_randomactivity;
defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot . '/mod/randomactivity/locallib.php');
require_once($CFG->dirroot . '/mod/randomactivity/lib.php');

/**
 * Module observer definition.
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class module_observer {
    /**
     * Observer called every time an activity grade or grade item changes.
     * Updates Random activities grades and grade items accordingly.
     * @param \core\event\user_graded $event
     */
    public static function user_activity_graded(\core\event\user_graded $event) {
        global $DB;
        $grade = $event->get_grade();
        $gradeitem = $grade->grade_item;

        if ($gradeitem->itemtype != 'mod' || $gradeitem->itemmodule == 'randomactivity') {
            // Do not reflect grades of other randomactivities to avoid inifinite recursion.
            // Such recursion is not supposed to happen anyway.
            return;
        }

        $courseid = $gradeitem->courseid;
        $userid = $grade->userid;

        $randomactivities = get_fast_modinfo($courseid, -1)->get_instances_of('randomactivity');
        $gradedcm = get_fast_modinfo($courseid, -1)->get_instances_of($gradeitem->itemmodule)[$gradeitem->iteminstance];
        foreach ($randomactivities as $ra) {
            $modinstance = $DB->get_record('randomactivity', [ 'id' => $ra->instance ]);
            if ($modinstance->grade != 0 && ($modinstance->duedate == 0 || $modinstance->duedate >= time())) {
                $assignedcmid = randomactivity_get_assigned_activity($ra, $modinstance->activities, $userid, $modinstance->seed);
                if ($assignedcmid == $gradedcm->id) {
                    // This Random activity has the graded module in its pool. User should have its grade updated.
                    randomactivity_update_grade($modinstance, $userid, $grade);
                }
            }
        }
    }

    /**
     * Observer called every time an activity completion is updated.
     * Updates Random activities completion accordingly.
     * @param \core\event\course_module_completion_updated $event
     */
    public static function user_activity_completion_updated(\core\event\course_module_completion_updated $event) {
        global $DB;
        $eventdata = $event->get_record_snapshot('course_modules_completion', $event->objectid);

        $userid = $event->relateduserid;

        $module = get_coursemodule_from_id(null, $eventdata->coursemoduleid);
        $course = get_course($module->course);
        $courseinfo = get_fast_modinfo($course, $userid);
        $completedcm = $courseinfo->get_cm($module->id);

        if ($completedcm->modname == 'randomactivity') {
            // Do not reflect completion of other randomactivities to avoid inifinite recursion.
            // Such recursion is not supposed to happen anyway.
            return;
        }

        $completion = new \completion_info($course);
        if (!$completion->is_enabled()) {
            return;
        }

        $randomactivities = $courseinfo->get_instances_of('randomactivity');
        foreach ($randomactivities as $ra) {
            if ($ra->completion != COMPLETION_TRACKING_AUTOMATIC ||
                    ($ra->completionexpected > 0 && $ra->completionexpected < time())) {
                continue;
            }
            $modinstance = $DB->get_record('randomactivity', [ 'id' => $ra->instance ]);
            if ($modinstance->completiontrackactivity && ($modinstance->duedate == 0 || $modinstance->duedate >= time())) {
                $assignedcmid = randomactivity_get_assigned_activity($ra, $modinstance->activities, $userid, $modinstance->seed);
                if ($assignedcmid == $completedcm->id) {
                    // This Random activity has the completed module in its pool. User should have its completion updated.
                    // Calling update_state with COMPLETION_UNKNOWN will call randomactivity_get_completion_state.
                    $completion->update_state($ra, COMPLETION_UNKNOWN, $userid);
                }
            }
        }
    }
}
