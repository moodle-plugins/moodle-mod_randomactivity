<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Database upgrade definition.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Performs database actions to upgrade from older versions, if required.
 * @param int $oldversion
 * @return boolean
 */
function xmldb_randomactivity_upgrade($oldversion) {
    global $DB;
    $dbman = $DB->get_manager();

    if ($oldversion < 2021031100) {
        $table = new xmldb_table('randomactivity');

        // Define field duedate to be added to randomactivity.
        $field = new xmldb_field('duedate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0, 'name');

        // Conditionally launch add field duedate.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Random activity savepoint reached.
        upgrade_mod_savepoint(true, 2021031100, 'randomactivity');
    }

    if ($oldversion < 2021052100) {
        $table = new xmldb_table('randomactivity');

        // Define field timemodified to be added to randomactivity.
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0, 'grade');

        // Conditionally launch add field timemodified.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Random activity savepoint reached.
        upgrade_mod_savepoint(true, 2021052100, 'randomactivity');
    }

    if ($oldversion < 2022071300) {
        $table = new xmldb_table('randomactivity');

        // Define field completiontrackactivity to be added to randomactivity.
        $field = new xmldb_field('completiontrackactivity', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, 0, 'grade');

        // Conditionally launch add field completiontrackactivity.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Random activity savepoint reached.
        upgrade_mod_savepoint(true, 2022071300, 'randomactivity');
    }

    return true;
}
