<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * List of every Random activity within current course.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/locallib.php');

require_login();

$id = required_param( 'id', PARAM_INT ); // Course ID.
$groupbyseeding = optional_param( 'groupbyseeding', 0, PARAM_BOOL );

global $DB, $PAGE, $OUTPUT;

$course = $DB->get_record( 'course', [ 'id' => $id ] );
$context = context_course::instance( $id );

require_capability('mod/randomactivity:viewactivities', $context);

$PAGE->set_course( $course );
$PAGE->set_context( $context );
$PAGE->set_title( $course->fullname . ' - ' . get_string( 'modulenameplural', RANDOMACTIVITY ) );
$PAGE->set_pagelayout( 'incourse' );
$PAGE->set_heading( $course->fullname );
$PAGE->navbar->add( get_string( 'modulenameplural', RANDOMACTIVITY ) );
$PAGE->set_url( '/mod/randomactivity/index.php', [ 'id' => $id ] );

echo $OUTPUT->header();
echo $OUTPUT->heading( $course->fullname . ' - ' . get_string( 'modulenameplural', RANDOMACTIVITY ) );
echo $OUTPUT->box_start();

echo '<div class="d-flex align-items-center">';
$url = $PAGE->url;
$urls = [
        $url->out(true, [ 'groupbyseeding' => 0 ]),
        $url->out(true, [ 'groupbyseeding' => 1 ]),
];
echo $OUTPUT->url_select( [
        $urls[0] => get_string( 'orderbyappearanceincourse', RANDOMACTIVITY ),
        $urls[1] => get_string( 'groupbyseeding', RANDOMACTIVITY ),
], $urls[$groupbyseeding], null );
echo $OUTPUT->help_icon('orderbyselect', RANDOMACTIVITY);
echo '</div><br>';


$i = 1;
$rows = [];
foreach (get_fast_modinfo($course, -1)->get_instances_of('randomactivity') as $cminfo) {
    $currentstatus = new stdClass();
    $message = '';
    $activities = [];
    $modinstance = $DB->get_record('randomactivity', [ 'id' => $cminfo->instance ]);
    if ($modinstance->activities != '') {
        $j = 1;
        foreach (explode(' ', $modinstance->activities) as $activityid) {
            try {
                $activitycminfo = get_fast_modinfo($course, -1)->get_cm($activityid);
            } catch (moodle_exception $e) {
                $activitycminfo = null;
            }
            $activity = randomactivity_activity_icon_and_name($activitycminfo);
            if ($groupbyseeding) {
                $activity = $j++ . ' - ' . $activity;
            }
            $activities[] = $activity;
            $activitystatus = randomactivity_get_activity_status($activitycminfo);
            if (!isset($currentstatus->severity) || $currentstatus->severity < $activitystatus->severity) {
                $currentstatus = $activitystatus;
            }
        }
        if ($currentstatus->severity > RANDOMACTIVITY_STATUS_OK) {
            $message = get_string( 'activityhasissue', RANDOMACTIVITY, $currentstatus->message );
        }
    } else {
        $currentstatus->severity = RANDOMACTIVITY_STATUS_ERROR;
        $message = get_string( 'noactivityerror', RANDOMACTIVITY );
    }

    switch ($currentstatus->severity) {
        case RANDOMACTIVITY_STATUS_WARNING: $chip = get_string('warning');
            break;
        case RANDOMACTIVITY_STATUS_ERROR: $chip = get_string('error');
            break;
        case RANDOMACTIVITY_STATUS_OK:
        default: $chip = get_string('ok');
            break;
    }
    $status = randomactivity_print_status_chip($currentstatus->severity, $chip, $message);
    $row = [ $i++, randomactivity_activity_icon_and_name($cminfo), implode('<br>', $activities), $modinstance->seed, $status ];

    if ($groupbyseeding) {
        srand( substr( $modinstance->seed, -9, 9 ) );
        $seeding = hash( 'md5', rand() + count( $activities ) );
        if (!isset($rows[$seeding])) {
            $rows[$seeding] = [];
        }
        $rows[$seeding][] = $row;
    } else {
        $rows[] = $row;
    }
}

$tableheading = [ '#', get_string('name'), get_string('activities'), get_string('seed', RANDOMACTIVITY), get_string('status') ];
$tablesize = [ '2em', '35%', null, '10%', '10%' ];

if ($groupbyseeding) {
    $i = 1;
    foreach ($rows as $seeding) {
        echo '<hr><b>' . get_string( 'seedingn', RANDOMACTIVITY, $i++ ) . '</b>';
        $table = new html_table();
        $table->head = $tableheading;
        $table->size = $tablesize;
        $table->data = $seeding;
        echo html_writer::table( $table );
    }
} else {
    $table = new html_table();
    $table->head = $tableheading;
    $table->size = $tablesize;
    $table->data = $rows;
    echo html_writer::table( $table );
}

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
