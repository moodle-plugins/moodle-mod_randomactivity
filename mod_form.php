<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines editing form for Random activity module.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

global $CFG;
require_once($CFG->dirroot . '/course/moodleform_mod.php');

/**
 * Random activity edition form definition.
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_randomactivity_mod_form extends moodleform_mod {
    /**
     * {@inheritdoc}
     */
    public function definition() {
        $mform = $this->_form;

        $mform->addElement( 'header', 'general', get_string( 'general', 'form' ) );
        $mform->addElement( 'text', 'name', get_string( 'name' ), [ 'size' => '50' ] );
        $mform->setType( 'name', PARAM_TEXT );
        $mform->addRule( 'name', null, 'required', null, 'client' );
        $mform->applyFilter( 'name', 'trim' );

        $mform->addElement( 'header', 'appearance', get_string( 'appearance' ) );
        $mform->setExpanded( 'appearance', true );
        $mform->addElement( 'selectyesno', 'dynamicdisplay', get_string( 'dynamicdisplay', 'mod_randomactivity' ) );
        $mform->setType( 'dynamicdisplay', PARAM_INT );
        $mform->addHelpButton( 'dynamicdisplay', 'dynamicdisplay', 'mod_randomactivity' );
        $mform->setDefault( 'dynamicdisplay', 1 );

        $mform->addElement( 'header', 'hduedate', get_string( 'duedate', 'mod_randomactivity' ) );
        $mform->addElement( 'date_time_selector', 'duedate', get_string( 'duedate', 'mod_randomactivity' ), [
                'optional' => true,
        ] );
        $mform->addHelpButton( 'duedate', 'duedate', 'mod_randomactivity' );
        $mform->setDefault( 'duedate', 0 );

        $this->standard_grading_coursemodule_elements();
        $this->standard_coursemodule_elements();

        $this->add_action_buttons();
    }

    /**
     * Add module-specific activity completion rules.
     * @return array Array of string IDs of added items.
     */
    public function add_completion_rules() {
        $mform =& $this->_form;

        $mform->addElement('advcheckbox', 'completiontrackactivity', get_string('completiontrackactivity', 'mod_randomactivity'),
                get_string('completiontrackactivity_desc', 'mod_randomactivity'));
        $mform->addHelpButton('completiontrackactivity', 'completiontrackactivity', 'mod_randomactivity');

        return [ 'completiontrackactivity' ];
    }

    /**
     * Called during validation. Indicates whether a module-specific completion rule is selected.
     *
     * @param array $data Input data (not yet validated)
     * @return bool True if one or more rules is enabled, false if none are.
     */
    public function completion_rule_enabled($data) {
        return !empty($data['completiontrackactivity']);
    }
}
