<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Grades view page.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/locallib.php');

require_login();

$id = required_param( 'id', PARAM_INT );
$userid = optional_param( 'userid', 0, PARAM_INT );

global $DB, $PAGE, $OUTPUT;

$cm = get_coursemodule_from_id( 'randomactivity', $id );
$course = $DB->get_record( 'course', [ 'id' => $cm->course ] );
$context = context_module::instance( $cm->id );
$module = $DB->get_record( 'randomactivity', [ 'id' => $cm->instance ] );

require_capability('mod/randomactivity:viewgrades', $context);

$PAGE->set_cm( $cm, $course, $module );
$PAGE->set_context( $context );
$PAGE->set_title( $module->name . ' - ' . get_string( 'grades' ) );
$PAGE->set_pagelayout( 'incourse' );
$PAGE->set_heading( $course->fullname );
$PAGE->set_url( '/mod/randomactivity/gradeslist.php', [ 'id' => $id ] );
$PAGE->force_settings_menu();

echo $OUTPUT->header();
echo $OUTPUT->heading( format_string( $module->name ) . ' - ' . get_string( 'grades' ) );

if ($module->grade == 0) {
    // This activity is not graded, this page is not relevant.
    echo $OUTPUT->notification(get_string('nogradesetup', RANDOMACTIVITY), \core\output\notification::NOTIFY_INFO);
    echo $OUTPUT->continue_button(new moodle_url('/mod/randomactivity/view.php', [ 'id' => $id ]));
    echo $OUTPUT->footer();
    die();
}

echo $OUTPUT->box_start();

// Create button link to gradebook.
$gradeitem = grade_item::fetch(
        [
                'itemtype' => 'mod',
                'itemmodule' => 'randomactivity',
                'iteminstance' => $cm->instance,
                'outcomeid' => null,
        ] );
$gradebookurl = new moodle_url('/grade/report/singleview/index.php',
        [ 'id' => $course->id, 'item' => 'grade', 'itemid' => $gradeitem->id ]);
echo '<a class="btn btn-secondary" href="' . $gradebookurl->out() . '">';
echo get_string( 'viewingradebook', RANDOMACTIVITY );
echo '</a>';

$table = new html_table();
$table->attributes['class'] = 'generaltable mt-3';

$table->head = [
        '#',
        get_string('name'),
        get_string('grade', 'core_grades'),
        get_string('assignedactivity', RANDOMACTIVITY),
        get_string('originalgrade', RANDOMACTIVITY),
];
$table->size = [
        '2em',
        null,
        null,
        null,
        null,
];

$cminfo = get_fast_modinfo($course, -1)->get_cm($cm->id);

if ($userid == 0) {
    $users = get_enrolled_users($context);
    $table->id = 'gradestable';
    $PAGE->requires->js_call_amd('mod_randomactivity/sorttable', 'makeSortable', [ 'gradestable', [ 0 ], 0 ]);
} else {
    $users = [ core_user::get_user($userid) ];
}
$i = 1;
foreach ($users as $user) {
    $username = '<span value="' . $user->lastname .'">' . fullname($user) . '</span>';
    if ( has_capability( 'mod/randomactivity:manage', $context, $user ) ) {
        $username = '<em>' . $username . '</em>';
    }
    $gradeobj = grade_get_grades($course->id, 'mod', 'randomactivity', $cm->instance, $user->id)->items[0]->grades[$user->id];
    $grade = '<span value="' . $gradeobj->grade .'">' . $gradeobj->str_long_grade . '</span>';
    $assignedcmid = randomactivity_get_assigned_activity($cminfo, $module->activities, $user->id, $module->seed);
    try {
        $assignedcm = get_fast_modinfo($course, -1)->get_cm($assignedcmid);
    } catch (moodle_exception $e) {
        // Some assigned modules might be corrupted, do not retrieve any grade from them.
        $table->data[] = [ $i++, $username, $grade, randomactivity_activity_icon_and_name(null), '-' ];
        continue;
    }
    $originalgradeitems = grade_get_grades($course->id, 'mod', $assignedcm->modname, $assignedcm->instance, $user->id)->items;
    if (count( $originalgradeitems ) == 0) {
        $originalgrade = get_string( 'notgraded', RANDOMACTIVITY );
    } else {
        $originalgradeobj = $originalgradeitems[0]->grades[$user->id];
        $originalgradestr = '<span value="' . $originalgradeobj->grade . '">' . $originalgradeobj->str_long_grade . '</span>';
        $urloriginalgrade = new moodle_url( '/mod/' . $assignedcm->modname . '/grade.php',
                [ 'id' => $assignedcmid , 'userid' => $user->id ] );
        $originalgrade = '<a href="' . $urloriginalgrade->out() . '">' . $originalgradestr . '</a>';
    }

    $table->data[] = [ $i++, $username, $grade, randomactivity_activity_icon_and_name($assignedcm), $originalgrade ];
}

echo html_writer::table( $table );

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
