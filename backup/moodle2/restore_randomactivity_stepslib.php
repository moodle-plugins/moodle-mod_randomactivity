<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Random activity restore structure lib for Moodle 2.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Restore structure definition for Random activity.
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_randomactivity_activity_structure_step extends restore_activity_structure_step {

    /**
     * {@inheritdoc}
     */
    protected function define_structure() {
        $paths = [ new restore_path_element('randomactivity', '/activity/randomactivity') ];

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    /**
     * {@inheritdoc}
     * @param stdClass $data Activity data to be put in DB.
     */
    protected function process_randomactivity($data) {
        global $DB;

        $randomactivity = (object)$data;
        $randomactivity->course = $this->get_courseid();

        $newitemid = $DB->insert_record('randomactivity', $randomactivity);
        $this->apply_activity_instance($newitemid);
    }
}
