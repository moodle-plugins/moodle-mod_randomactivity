<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Random activity backup structure lib for Moodle 2.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Backup structure definition for Random activity.
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class backup_randomactivity_activity_structure_step extends backup_activity_structure_step {

    /**
     * {@inheritdoc}
     */
    protected function define_structure() {
        $randomactivity = new backup_nested_element('randomactivity', [ 'id' ],
                [ 'name', 'duedate', 'activities', 'seed', 'dynamicdisplay', 'grade', 'timemodified' ]);

        $randomactivity->set_source_table('randomactivity', [ 'id' => backup::VAR_ACTIVITYID ]);

        // Return the root element wrapped into standard activity structure.
        return $this->prepare_activity_structure($randomactivity);
    }
}
