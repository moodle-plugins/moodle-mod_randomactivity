<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Random activity restore task lib for Moodle 2.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die ();
require_once(dirname(__FILE__) . '/restore_randomactivity_stepslib.php');

/**
 * Restore task for Random activity.
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_randomactivity_activity_task extends restore_activity_task {
    /**
     * Define particular restore settings for this activity.
     */
    protected function define_my_settings() {
        // No particular restore settings.
    }

    /**
     * {@inheritdoc}
     */
    protected function define_my_steps() {
        $this->add_step( new restore_randomactivity_activity_structure_step ( 'randomactivity_structure', 'randomactivity.xml' ) );
    }

    /**
     * {@inheritdoc}
     */
    public static function define_decode_contents() {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function define_decode_rules() {
        return [
                new restore_decode_rule ( 'RANDOMACTIVITYVIEW', '/mod/randomactivity/view.php?id=$1', 'course_module' ),
                new restore_decode_rule ( 'RANDOMACTIVITYGRADES', '/mod/randomactivity/gradeslist.php?id=$1', 'course_module' ),
                new restore_decode_rule ( 'RANDOMACTIVITYINDEX', '/mod/randomactivity/index.php?id=$1', 'course' ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function define_restore_log_rules() {
        return [];
    }
}
