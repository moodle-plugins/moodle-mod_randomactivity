<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Random activity backup task lib for Moodle 2.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once(dirname(__FILE__) . '/backup_randomactivity_stepslib.php');

/**
 * Backup task for Random activity.
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class backup_randomactivity_activity_task extends backup_activity_task {

    /**
     * Define particular backup settings for this activity.
     */
    protected function define_my_settings() {
        // No particular backup settings.
    }

    /**
     * {@inheritdoc}
     */
    protected function define_my_steps() {
        $this->add_step( new backup_randomactivity_activity_structure_step ( 'randomactivity_structure', 'randomactivity.xml' ) );
    }

    /**
     * {@inheritdoc}
     * @param string $content some HTML text that eventually contains URLs to the activity instance scripts
     */
    public static function encode_content_links($content) {
        $search = "/(\/mod\/randomactivity\/index.php\?id\=)([0-9]+)/";
        $content = preg_replace ( $search, '$@RANDOMACTIVITYINDEX*$2@$', $content );

        $search = "/(\/mod\/randomactivity\/view.php\?id\=)([0-9]+)/";
        $content = preg_replace ( $search, '$@RANDOMACTIVITYVIEW*$2@$', $content );

        $search = "/(\/mod\/randomactivity\/gradeslist.php\?id\=)([0-9]+)/";
        $content = preg_replace ( $search, '$@RANDOMACTIVITYGRADES*$2@$', $content );

        return $content;
    }
}
