<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lib for Random activity type.
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('RANDOMACTIVITY', 'mod_randomactivity');

define('RANDOMACTIVITY_STATUS_OK', 0);
define('RANDOMACTIVITY_STATUS_WARNING', 1);
define('RANDOMACTIVITY_STATUS_ERROR', 2);

/**
 * Returns the activity an user is assigned to.
 * @param cm_info $cminfo Random activity course module info.
 * @param string|array $activities The activity pool from which to draw.
 * @param int $userid User id.
 * @param int|string $seed Seed used for attribution.
 * @return int|null Assigned course module id or null if none.
 */
function randomactivity_get_assigned_activity($cminfo, $activities, $userid, $seed) {
    if (!$activities) {
        return null;
    }
    if (is_string($activities)) {
        $activities = explode(' ', $activities);
    }
    $effectiveuserid = $userid;
    if ($cminfo->effectivegroupmode != NOGROUPS) {
        $groups = groups_get_all_groups($cminfo->course, $userid, $cminfo->groupingid);
        if (count($groups) == 1) {
            // Assign user with their group only if they are part of exactly one group.
            $effectiveuserid = array_shift($groups)->id;
        }
    }
    srand( substr( $seed, -9, 9 ) ); // Trim the seed to an int that is not too big.
    $x = hexdec( substr( hash( 'md5', rand() + $effectiveuserid ), 0, 4 ) );
    return $activities[$x % count( $activities )];
}

/**
 * Returns the list of assignees for every activity in a pool.
 * @param string|array $activities The activity pool.
 * @param int|string $seed Seed used for attribution.
 * @param context $context Course module context.
 * @param int $groupid Group id - if provided, only members of this group will be considered.
 * @return array Assignees for every activity, indexed by cmid.
 */
function randomactivity_get_assignees($activities, $seed, $context, $groupid = 0) {
    $assignees = [];
    $assignedgroups = [];
    if ($activities != '') {
        $cminfo = get_fast_modinfo($context->get_course_context()->instanceid, -1)->get_cm($context->instanceid);
        foreach (get_enrolled_users($context) as $user) {
            if (!has_capability('mod/randomactivity:manage', $context, $user)
                    && !has_capability('mod/randomactivity:viewactivities', $context, $user)
                    && ($groupid == 0 || groups_is_member($groupid, $user->id))) {
                $assignedcm = randomactivity_get_assigned_activity($cminfo, $activities, $user->id, $seed);
                if (! isset($assignees[$assignedcm])) {
                    $assignees[$assignedcm] = [];
                }
                if ($cminfo->effectivegroupmode == NOGROUPS) {
                    // Assign user individually.
                    $assignees[$assignedcm][] = fullname($user);
                } else {
                    // This Random activity is set to use groups.
                    $groups = groups_get_all_groups($cminfo->course, $user->id, $cminfo->groupingid);
                    if (count($groups) == 1) {
                        // User belongs to exactly one group: assign them with their group.
                        $group = array_shift($groups);
                        if (! isset($assignedgroups[$assignedcm])) {
                            $assignedgroups[$assignedcm] = [];
                        }
                        $assignedgroups[$assignedcm][$group->id] = $group->name;
                    } else {
                        // User belongs to no group or more than one group: assign them individually.
                        $assignees[$assignedcm][] = fullname($user);
                    }
                }
            }
        }
        // Process assigned groups for a nice display.
        foreach ($assignedgroups as $cmid => $cmgroups) {
            foreach ($cmgroups as $id => $groupname) {
                $groupmembers = [];
                foreach (groups_get_members($id) as $member) {
                    $groupmembers[] = fullname($member);
                }
                $assignees[$cmid][] = '<span class="group-assignee" title="' . implode(', ', $groupmembers) . '">' .
                                          '<i class="fa fa-fw fa-group"></i>&nbsp;' . $groupname .
                                      '</span>';
            }
        }
    }
    return $assignees;
}

/**
 * Returns the list of assignees for every activity in a pool, formatted to be displayed for example within a table.
 * @param string|array $activities The activity pool.
 * @param int|string $seed Seed used for attribution.
 * @param context $context Course module context.
 * @param int $groupid Group id - if provided, only members of this group will be considered.
 * @return array Assignees for every activity.
 */
function randomactivity_get_assignees_formatted($activities, $seed, $context, $groupid = 0) {
    $ret = [];
    if ($activities != '') {
        $assignees = randomactivity_get_assignees($activities, $seed, $context, $groupid);
        foreach (explode(' ', $activities) as $cmid) {
            if (! isset($assignees[$cmid])) {
                $cmassignees = get_string('none');
            } else {
                $limit = 6;
                $cmassignees = '<b>(' . count($assignees[$cmid]) . ')</b> ' .
                                implode(', ', array_slice($assignees[$cmid], 0, $limit));
                if (count($assignees[$cmid]) > $limit) {
                    $additionalassignees = implode(', ', array_slice($assignees[$cmid], $limit));
                    $linkaction = 'event.preventDefault();this.nextSibling.style.display=\'\';this.remove();';
                    $cmassignees .= ', <a href="#" onclick="' . $linkaction . '">' .
                                    '... (' . get_string('viewall', RANDOMACTIVITY) . ')' .
                                    '</a>' .
                                    '<span style="display:none;">' . $additionalassignees . '</span>';
                }
            }

            $ret[] = $cmassignees;
        }
    }
    return $ret;
}

/**
 * Returns HTML with icon and name for a given course module.
 * @param cm_info|null $cminfo Course module info.
 * @param boolean $withlink Whether to create link to activity view page.
 * @return string HTML fragment.
 */
function randomactivity_activity_icon_and_name($cminfo, $withlink = true) {
    if ($cminfo !== null) {
        $icon = '<img class="activityicon" src="' . $cminfo->get_icon_url()->out() . '"/>';
        $name = '<span class="activity">' . $icon . $cminfo->get_formatted_name() . '</span>';
        if ($withlink && $cminfo->url !== null) {
            $name = '<a href="' . $cminfo->url->out() . '">' . $name . '</a>';
        }
    } else {
        $icon = randomactivity_print_icon( 'warning' );
        $name = '<span class="text-danger">' . $icon . get_string('unknown', RANDOMACTIVITY) . '</span>';
    }
    return $name;
}

/**
 * Returns HTML of Random activity Fontawesome icon.
 * @param string $icon Icon name.
 * @param string $title Icon title.
 * @param array $attributes HTML attributes.
 * @return string HTML fragment.
 */
function randomactivity_print_icon($icon, $title = '', $attributes = []) {
    global $OUTPUT;
    return $OUTPUT->pix_icon($icon, $title, RANDOMACTIVITY, $attributes);
}

/**
 * Updates grade for a given user, from an other activity grade (typically user's assigned activity grade).
 * @param stdClass $modinstance Database record of Random activity.
 * @param int $userid User id.
 * @param grade_grade $grade Original grade from which to update grade.
 */
function randomactivity_update_grade($modinstance, $userid, grade_grade $grade) {
    $grade = clone($grade); // It is important not to modify $grade, as it may be re-used for other Random activites.
    $adjustedgrade = ($grade->rawgrade - $grade->rawgrademin) / ($grade->rawgrademax - $grade->rawgrademin);
    $grade->rawgrade = randomactivity_compute_grade_from($modinstance, $adjustedgrade);
    grade_update( 'mod/randomactivity', $modinstance->course, 'mod', 'randomactivity', $modinstance->id, 0, $grade, null );
}

/**
 * Computes and returns a Random activity grade from an other grade.
 * @param stdClass $modinstance Database record of Random activity.
 * @param float $grade Original rade between 0 and 1.
 * @return number|null Computed grade for Random activity, null if not graded.
 */
function randomactivity_compute_grade_from($modinstance, $grade) {
    $gradeitems = grade_get_grades($modinstance->course, 'mod', 'randomactivity', $modinstance->id)->items;
    if (count( $gradeitems ) == 0) {
        return null;
    }
    return $grade * ($gradeitems[0]->grademax - $gradeitems[0]->grademin) + $gradeitems[0]->grademin;
}

/**
 * Checks and returns a course module status to be used by a Random activity.
 * @param cm_info|null $cminfo Course module info.
 * @return stdClass Status with message and severity fields. The latter is one of the RANDOMACTIVITY_STATUS_xx constants.
 */
function randomactivity_get_activity_status($cminfo) {
    $status = new stdClass();
    $status->message = get_string('ok');
    $status->severity = RANDOMACTIVITY_STATUS_OK;
    if ($cminfo === null) {
        $status->message = get_string('modulenotfound', RANDOMACTIVITY);
        $status->severity = RANDOMACTIVITY_STATUS_ERROR;
    } else if ($cminfo->url === null) {
        // No view page.
        $status->message = get_string('notsupported', RANDOMACTIVITY);
        $status->severity = RANDOMACTIVITY_STATUS_WARNING;
    } else if (!$cminfo->visible) {
        // Not available.
        $status->message = get_string('notavailable');
        $status->severity = RANDOMACTIVITY_STATUS_WARNING;
    }
    return $status;
}

/**
 * Return HTML to display an activity status chip.
 * @param stdClass|int $status Status from randomactivity_get_activity_status(),
 *  or severity level as one of the RANDOMACTIVITY_STATUS_xx constants.
 * @param string $message (optional) Message to display if $status is severity level.
 * @param string $title (optional) Chip title.
 * @return string HTML fragment.
 */
function randomactivity_print_status_chip($status, $message = '', $title = '') {
    if (is_object($status)) {
        $severity = $status->severity;
        $message = $status->message;
    } else {
        $severity = $status;
    }
    switch ($severity) {
        case RANDOMACTIVITY_STATUS_WARNING: $statusclass = 'badge-warning';
            break;
        case RANDOMACTIVITY_STATUS_ERROR: $statusclass = 'badge-important badge-danger';
            break;
        case RANDOMACTIVITY_STATUS_OK:
        default: $statusclass = 'badge-success';
            break;
    }
    return '<span class="badge ' . $statusclass . '" title="' . $title . '">' . $message . '</span>';
}

/**
 * Creates a row with an activity data to be put in an html_table.
 * @param int $i Index of the row.
 * @param cm_info|null $cminfo Course module info.
 * @param string $cmassignees Formatted assignees list.
 * @param boolean $editmode Whether the table/row can be edited.
 * @param int|null $cmid (optional) Needed if $cminfo is null.
 * @return html_table_row The built table row.
 */
function randomactivity_create_activity_table_row($i, $cminfo, $cmassignees, $editmode, $cmid = null) {
    global $CFG, $DB;
    $status = randomactivity_print_status_chip(randomactivity_get_activity_status($cminfo));

    if ($cminfo !== null) {
        $cmid = $cminfo->id;
        $actions = '<a href="' . $CFG->wwwroot . '/course/modedit.php?update=' . $cminfo->id . '">' .
                        randomactivity_print_icon('settings', get_string('editsettings'), [ 'class' => 'text-dark mr-2' ]) .
                    '</a>';
        if ($editmode) {
            $actions .= randomactivity_print_icon('move', get_string('move'), [ 'class' => 'draghandle clickable mr-2' ]);
        }

        $gradeitems = grade_get_grades($cminfo->course, 'mod', $cminfo->modname, $cminfo->instance)->items;
        if (count($gradeitems) == 0) {
            // No grade.
            $grade = get_string( 'nograde' );
        } else if ($gradeitems[0]->scaleid != 0) {
            // Scale.
            $grade = $DB->get_record('scale', [ 'id' => $gradeitems[0]->scaleid ])->name;
        } else {
            // Point.
            $grade = get_string( 'modgradetypepoint', 'grades' );
        }
    } else {
        $grade = '';
        $actions = '';
    }

    if ($editmode) {
        $actions .= randomactivity_print_icon('remove', get_string('remove'), [ 'class' => 'removebutton clickable' ]);
    }

    $row = new html_table_row([ $i, randomactivity_activity_icon_and_name($cminfo), $cmassignees, $grade, $status, $actions ]);
    $row->attributes['data-cmid'] = $cmid;
    return $row;
}

/**
 * Creates an object to be used as a calendar event for the given Random activity.
 * @param stdClass $modinstance Database record of Random activity.
 * @return stdClass Object to be passed to calendar_event::create() or calendar_event::load().
 */
function randomactivity_get_calendar_event($modinstance) {
    $event = new stdClass();
    $event->name = get_string('duedatefor', RANDOMACTIVITY, $modinstance->name);
    $event->description = get_string('duedatefor_desc', RANDOMACTIVITY);
    $event->format = FORMAT_HTML;
    $event->courseid = $modinstance->course;
    $event->userid = 0;
    $event->groupid = 0;
    $event->modulename = 'randomactivity';
    $event->instance = $modinstance->id;
    $event->eventtype = 'duedate';
    $event->timestart = $modinstance->duedate;
    $event->type = CALENDAR_EVENT_TYPE_ACTION;
    $event->timesort = $modinstance->duedate;
    return $event;
}
