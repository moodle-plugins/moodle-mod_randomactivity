<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Grade view redirection (typically from gradebook).
 * @package    mod_randomactivity
 * @copyright  Astor Bizard, 2020
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../config.php');

require_login();

$id = required_param( 'id', PARAM_INT );
$userid = optional_param('userid', 0, PARAM_INT);

$cm = get_coursemodule_from_id( 'randomactivity', $id );
$context = context_module::instance( $cm->id );

if ( !has_capability('mod/randomactivity:viewgrades', $context) ) {
    redirect( new moodle_url('/mod/randomactivity/view.php', [ 'id' => $id ]) );
} else {
    redirect( new moodle_url('/mod/randomactivity/gradeslist.php', [ 'id' => $id, 'userid' => $userid ]) );
}
die();
